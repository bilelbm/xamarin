﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Data
{
    public interface INetworkConnection
    {
        bool IsConnected { get; }
        void CheckNetworkconnection();
    }
}
