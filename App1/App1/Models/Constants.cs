﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1.Models
{
    public class Constants
    {
        public static bool IsDev = true;

        public static Color Backgroundcolor = Color.FromRgb(58, 153, 215);

        public static Color MainTextcolor = Color.White;

        public static int LoginIconHeight = 120;


        //---------Login---------------
        public static string LoginUrl = "https://kanalabs.com/projects/phone_vault/api.php?f=insert";

        public static string NoInternetText = "No Internet, Please reconnect";
    }
}
