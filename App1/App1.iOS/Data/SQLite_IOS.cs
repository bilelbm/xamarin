﻿using App1.Data;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using App1.iOS.Data;

[assembly: Dependency(typeof(SQLite_IOS))]

namespace App1.iOS.Data
{
    public class SQLite_IOS : ISQLite
    {
        public SQLite_IOS() { }
        public SQLite.SQLiteConnection GetConnection()
        {
            var fileName = "Testdb.dba";
            var documentPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var libraryPath = Path.Combine(documentPath, "..", "Library");
            var path = Path.Combine(libraryPath, fileName);
            var connection = new SQLite.SQLiteConnection(path);
            return connection;
        }
    }
}
